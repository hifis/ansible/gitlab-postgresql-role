# GitLab PostgreSQL Role

[![pipeline status](https://gitlab.com/hifis/ansible/gitlab-postgresql-role/badges/master/pipeline.svg)](https://gitlab.com/hifis/ansible/gitlab-postgresql-role/-/commits/master)

This role provides a standalone PostgreSQL server for GitLab.

## Requirements

None.

## Role Variables

```yaml
postgresql_listen_address: "0.0.0.0"
```
Specifies the TCP/IP address on which the server is to listen for connections.

```yaml
postgresql_port: "5432"
```
The TCP port the server listens on.

```yaml
postgresql_password_hash: "2a3bbf56a46856ee342f9025c4c342ad"
```
A MD5 password hash for PostgreSQL user `gitlab`. The default password is
`change_me`.


## Dependencies

- [geerlingguy.gitlab](https://galaxy.ansible.com/geerlingguy/gitlab)

## Example Playbook

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:
```yaml
- hosts: servers
  roles:
     - { role: username.rolename, x: 42 }
```

## License

[Apache-2.0](LICENSES/Apache-2.0.txt)

## Author Information

This role was created by [HIFIS Software Services](https://software.hifis.net/).
